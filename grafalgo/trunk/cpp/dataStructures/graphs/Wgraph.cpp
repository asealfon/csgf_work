/** @file Wgraph.cpp
 *
 *  @author Jon Turner
 *  @date 2011
 *  This is open source software licensed under the Apache 2.0 license.
 *  See http://www.apache.org/licenses/LICENSE-2.0 for details.
 */

#include "stdinc.h"
#include "Wgraph.h"

namespace grafalgo {

/** Construct a Wgraph with space for a specified number of vertices and edges.
 *  @param numv is number of vertices in the graph
 *  @param maxe is the maximum number of edges
 */
Wgraph::Wgraph(int numv, int maxe) : Graph(numv,maxe) {
	makeSpace(numv, maxe);
}

/** Free space used by Wgraph */
Wgraph::~Wgraph() { freeSpace(); }

/** Allocate and initialize dynamic storage for Wgraph.
 *  @param numv is the number of vertices to allocate space for
 *  @param maxe is the number of edges to allocate space for
 */
void Wgraph::makeSpace(int numv, int maxe) { wt = new int[maxe+1]; }

/** Free space used by graph. */
void Wgraph::freeSpace() { delete [] wt; }

/** Resize a Wgraph object.
 *  The old value is discarded.
 *  @param numv is the number of vertices to allocate space for
 *  @param maxe is the number of edges to allocate space for
 */
void Wgraph::resize(int numv, int maxe) {
	freeSpace(); Graph::resize(numv,maxe); makeSpace(numv,maxe); 
}

/** Expand the space available for this Wgraph.
 *  Rebuilds old value in new space.
 *  @param size is the size of the resized object.
 */
void Wgraph::expand(int numv, int maxe) {
	if (numv <= n() && maxe <= M()) return;
	Wgraph old(this->n(),this->M()); old.copyFrom(*this);
	resize(numv,maxe); this->copyFrom(old);
}

/** Copy into list from source. */
void Wgraph::copyFrom(const Wgraph& source) {
	if (&source == this) return;
	if (source.n() > n() || source.m() > M())
		resize(source.n(),source.m());
	else clear();
	for (edge e = source.first(); e != 0; e = source.next(e)) {
		joinWith(source.left(e),source.right(e),e);
		setWeight(e,source.weight(e));
	}
        sortAdjLists();
}

/** Determine the total weight of a list of edges
 *  @param elist is a list of edge numbers
 *  @return the sume of the edge weights for the edges in elist
 */
edgeWeight Wgraph::weight(Glist<edge> elist) const {
	edgeWeight sum = 0;
	for (index x = elist.first(); x != 0; x = elist.next(x))
		sum += weight(elist.value(x));
	return sum;
}

/** Read adjacency list from an input stream, add it to the graph.
 *  @param in is an open input stream
 *  @return true on success, false on error.
 */
bool Wgraph::readAdjList(istream& in) {
	if (!Util::verify(in,'[')) return false;
	vertex u;
	if (!Adt::readIndex(in,u)) return false;
	if (u > n()) expand(u,m());
	if (!Util::verify(in,':')) return false;
	while (in.good() && !Util::verify(in,']'))
    {
		vertex v; edge e;
		if (!Adt::readIndex(in,v)) return false;

		if (v > n()) expand(v,m());
		if (m() >= M()) expand(n(),max(1,2*m()));
		if (!Util::verify(in,'#'))
        {
			if (u < v) e = join(u,v);
		}
        else
        {
			if (!Util::readInt(in,e)) return false;
			if (e >= M()) expand(n(),e);
			if (u < v) {
				if (joinWith(u,v,e) != e) return false;
			} else {
				if ((u == left(e)  && v != right(e)) ||
				    (u == right(e) && v != left(e)))
					return false;
			}
		}
		int w;
		//if (!Util::verify(in,'(') || !Util::readInt(in,w) ||
		//    !Util::verify(in,')'))
        if (!Util::readInt(in,w))
			return false;
		if (u < v) setWeight(e,w);
	}
	return in.good();
}
    
    /** Read adjacency list from an input stream, add it to the graph.
     *  @param in is an open input stream
     *  @return true on success, false on error.
     */
    bool Wgraph::readMtxMarket(char *fname)
    {
        
        
        long count=0;
        long inp, type, sym;
        long numRow, numCol, nonZeros;
        double f;
        string s;
        ifstream inf;
        inf.open(fname, ios::in);
        if(inf.is_open())
        {
            size_t found1, found2, found3;
            getline(inf,s);
            found1 = s.find("pattern");
            if (found1 != string::npos)
                type = 2;
            else
                type = 3;
            found1 = s.find("symmetric");
            found2 = s.find("hermitian");
            found3 = s.find("skew-symmetric");
            if (found1 != string::npos || found2 != string::npos || found3 != string::npos)
                sym = 1;
            else
                sym = 0;
            while(inf.peek()=='%')
                getline(inf,s);
            
            inf>>inp;
            numRow=inp;
            inf>>inp;
            numCol=inp;
            inf>>inp;
            nonZeros=inp;
            
            count=inp;
            
            // read file and store edges (one direction only)
            int u, v;
            vector<vector<int> > graphCRSIdx(numRow);
            vector<vector<double> > graphCRSVal(numRow);
            int diag=0;
            while(count>0)
            {
                inf>>u;
                inf>>v;
                graphCRSIdx[u-1].push_back(numRow+v-1);
                if(type==3)
                {
                    inf>>f;
                    f = f;
                    graphCRSVal[u-1].push_back(f);
                }
                
                if (sym && u != v)
                {
                    graphCRSIdx[v-1].push_back(numRow+u-1);
                    if(type==3)
                        graphCRSVal[v-1].push_back(f);
                }
                if(u==v)
                    diag++;
                count--;
            }
            inf.close();
            
            int nVertices = numRow + numCol;
            int nEdges = nonZeros;
            if(sym == 1) //symmetric matrix
                nEdges = nonZeros*2 - diag;
            
            expand(nVertices, nEdges);
            for(int i=0; i<numRow; i++)
            {
                for(int j=0; j<graphCRSIdx[i].size(); j++)
                {
                    u = i+1;
                    v = graphCRSIdx[i][j]+1;
                    //if (u > n()) expand(u,m());
                    //if (v > n()) expand(v,m());
                    if (m() >= M()) expand(n(),max(1,2*m()));
                    edge e;
                    e = join(u, v);
                    int w = (int) graphCRSVal[i][j];
                    setWeight(e,w);
                }
                
            }
            
        }
        else
        {
            printf("file can not be opened \n");
            return false;
        }
        
        
        return true;
    }
    

    
    /** Read adjacency list from an input stream, add it to the graph.
     *  @param in is an open input stream
     *  @return true on success, false on error.
     */
    /*
    bool Wgraph::readMtxMarket(char *fname)
    {
      
        
        long count=0;
        long inp, type, sym;
        long numRow, numCol, nonZeros;
        double f;
        string s;
        ifstream inf;
        inf.open(fname, ios::in);
        if(inf.is_open())
        {
            size_t found1, found2, found3;
            getline(inf,s);
            found1 = s.find("pattern");
            if (found1 != string::npos)
                type = 2;
            else
                type = 3;
            found1 = s.find("symmetric");
            found2 = s.find("hermitian");
            found3 = s.find("skew-symmetric");
            if (found1 != string::npos || found2 != string::npos || found3 != string::npos)
                sym = 1;
            else
                sym = 0;
            while(inf.peek()=='%')
                getline(inf,s);
            
            inf>>inp;
            numRow=inp;
            inf>>inp;
            numCol=inp;
            inf>>inp;
            nonZeros=inp;
            
            count=inp;
            
            int u, v;
            while(count>0)
            {
                inf>>u;
                inf>>v;
                v += numRow;
                if(type==3)
                {
                    inf>>f;
                }
                
                if (u > n()) expand(u,m());
                if (v > n()) expand(v,m());
                if (m() >= M()) expand(n(),max(1,2*m()));
                
                edge e;
                e = join(u,v);
                int w = (int) f;
                setWeight(e,w);
                
                
                if (sym && u != v)
                {
                    if (v < u)
                    {
                        e = join(v-numRow,u+numRow);
                        setWeight(e,w);
                    }
                }
                count--;
            }
            inf.close();
            
     
        }
        else
        {
            printf("file can not be opened \n");
            return false;
        }


        return true;
    }
    */

/** Create a string representation of an edge.
 *  @param e is an edge number
 *  @param u is one of the endponts of e; it will appear first in the string
 *  @return the string
 */
string Wgraph::edge2string(edge e, vertex u) const {
	string s;
        vertex v = mate(u,e);
        s += "(" + index2string(u);
	s += "," + index2string(v) + "," + to_string(weight(e)) + ")";
	if (shoEnum) s += "#" + to_string(e);
        return s;
}

/** Create a string representation of an adjacency list.
 *  @param u is a vertex number
 *  @return the string
 */
string Wgraph::adjList2string(vertex u) const {
	string s;
	if (firstAt(u) == 0) return s;
	int cnt = 0;
	s += "[" + Adt::index2string(u) + ":";
	for (edge e = firstAt(u); e != 0; e = nextAt(u,e)) {
		vertex v = mate(u,e);
		s +=  " " + index2string(v);
		if (shoEnum) s += "#" + to_string(e);
		s += "(" + to_string(weight(e)) + ")";
		if (++cnt >= 15 && nextAt(u,e) != 0) {
			s +=  "\n"; cnt = 0;
		}
	}
	s +=  "]\n";
	return s;
}

/** Construct a string in dot file format representation 
 * of the Weighted Graph object.
 *  For small graphs (at most 26 vertices), vertices are
 *  represented in the string as lower case letters.
 *  For larger graphs, vertices are represented by integers.
 *  @return the string
 */
string Wgraph::toDotString() const {
	string s = "graph G {\n";
	int cnt = 0;
	for (edge e = first(); e != 0; e = next(e)) {
		vertex u = min(left(e),right(e));
		vertex v = max(left(e),right(e));
		s += Adt::index2string(u) + " -- ";
		s += Adt::index2string(v);
		s += " [label = \" " + to_string(weight(e)) + " \"] ; "; 
		if (++cnt == 10) { s += "\n"; cnt = 0; }
	}
	s += "}\n\n";
	return s;
}

} // ends namespace
