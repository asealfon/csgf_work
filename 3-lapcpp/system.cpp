// System dependent routines
// File: system.cpp

#include "system.h"
#include <stdlib.h>
#include <ctime>

void seedRandom(unsigned int seed)
// seed for random number generator.
{
  srand(seed);
  return;   
}
	      
double rrandom(void) /* modified name from random to rrandom */
// random number between 0.0 and 1.0 (uncluded).
{
  double rrr;
  
  rrr = (double) rand() / (double) RAND_MAX;
  return rrr;
}
 
double seconds()
// cpu time in seconds since start of run.
// WARNING: was machine dependant. On my machine, actually
// reported time in ms. This is fixed now.
{
  double secs;
   
  //secs = (double)(clock() / 1000.0);
  secs = (double) (clock() / (double) CLOCKS_PER_SEC);
  return(secs);
}

