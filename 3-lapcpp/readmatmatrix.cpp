#include <stdio.h>
#include "lap.h"
using namespace std;

#define MAXL 200

/*
 * filename = name of file from which to read matrix
 * startd = pointer to number of rows
 * endd = pointer to number of cols 
 * mode = how to transform matrix. The following modes are allowed:
 *   0: Find a min cost matching, treating missing edges as weight inf
 *   1: Find a max cost matching by negating all weights and running as in mode 0
 *   2: Find a min cost matching, taking the absolute value of all edge weights
 *   3: Find a max cost matching for absolute values, taking the negative and running as in mode 1. 
 */
cost **read_sparsemat_matrix(const char *filename, int *startd, int *endd, int mode) {
  int i,j;
  cost w;
  int N, M, E;
  FILE *fp;
  cost maxentry;
  cost ** assignmatrix;
  cost sentinel = -9999999;
  char line[MAXL];
  fp = fopen(filename, "r");
  maxentry = 0;
  w=0;

  if(fp == NULL) {
    printf("Could not open file.\n");
    return 0;
  }

  //printf("File opened\n");

  while(fgets(line, MAXL-1, fp) != NULL) { 
    /*if(++i < 10) printf("%s\n",line);*/
    if(line[0]=='%') continue;
    sscanf(line, "%d %d %d", &N, &M, &E);
    break;
  }
  *startd=N;
  *endd=M;
  printf("N=%d, M=%d, E=%d\n",N,M,E);
  if(N!=M) printf("WARNING: THIS PROGRAM EXPECTS A SQUARE MATRIX.");
  
  assignmatrix = new cost*[M];
  for(i=0; i<M; i++) {
    assignmatrix[i] = new cost[M];
    for(j=0; j<M; j++) {
      assignmatrix[i][j] = sentinel;
    }
  }

  //printf("Here2\n");

  while(fgets(line, MAXL-1, fp) != NULL) {
    /* Process edge weights */
    sscanf(line, "%d %d %ld", &i, &j, &w);
    assignmatrix[i-1][j-1] = w;
    if(w==sentinel)
      printf("WARNING: input had weight equal to sentinel value. Change sentinel in readmatmatrix.cpp.\n");  
    if(w<0) w= (-w);
    if(w > maxentry) maxentry = w;
    //printf("Here at %d %d %d\n",i,j,w); 
  }


  for(i=0; i<M; i++) {
    for(j=0; j<M; j++) {
      if(assignmatrix[i][j]==sentinel) {
        assignmatrix[i][j] = 2*M*maxentry + 1;
      }
      else {
        if( (mode == 2 || mode == 3) && assignmatrix[i][j] < 0 ) assignmatrix[i][j] = -assignmatrix[i][j];  
        if(mode == 1 || mode == 3) assignmatrix[i][j] = - assignmatrix[i][j];
      }
      //printf("%d ", assignmatrix[i][j]);
    }
    //printf("\n");
  }

  printf("If output is larger than %ld, no matching was found", M * maxentry+1);

  return assignmatrix;
  /*
  if(file.is_open()) {
    while(getline(file,line) && ++i < 10) {
      cout << line << '\n';
    }
    file.close();
  }
  else cout << "Couldn\'t open file.\n";
  return NULL;
  */
} 
/*
int main() {
  const char *fn = "CAG_mat72.mtx";
  read_sparsemat_matrix(fn);
  return 0;
}*/
