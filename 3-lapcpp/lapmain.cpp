/************************************************************************
*
*  lapmain.cpp
   version 1.0 - 4 September 1996
   author: Roy Jonker @ MagicLogic Optimization Inc.
   
   main program file to run and check Jonker-Volgenant LAP code
*
*************************************************************************/

#include "system.h"
#include "gnrl.h"
#include "lap.h"
#include "readmatmatrix.h"
#include <cstdlib>

int /* modified */  
main(int argc, char *argv[])
{
  #define COSTRANGE 1000.0
  #define PRINTCOST 0

  int dim, startdim, enddim;
  cost **assigncost, *u, *v, lapcost;
  row i, *colsol;
  col j, *rowsol;
  double runtime;
  int mode;

  //printf("start dimension ?\n");
  //scanf("%d", &startdim);
  //printf("end dimension ?\n");
  //scanf("%d", &enddim);
  startdim = 10;
  enddim = 10;

  //printf("\ndimensions %d .. %d\n", startdim, enddim);
 
  /*assigncost = new cost*[enddim];
  for (i = 0; i < enddim; i++)
    assigncost[i] = new cost[enddim];
  */

  if(argc < 2) {
    printf("Must be called with filename as argument.\n");
    exit(0);
  }
  printf("Filename %s\n",argv[1]);
  if(argc > 2) {
    mode = atoi(argv[2]);
    printf("Running in mode %d as specified in readmatmatrix.cpp.\n",mode); 
  }
  else {
    mode = 0;
    printf("No mode specified; running in mode 0 as specified in readmatmatrix.cpp.\n");
  }
  assigncost = read_sparsemat_matrix(argv[1], &startdim, &enddim, mode);


  rowsol = new col[enddim];
  colsol = new row[enddim];
  u = new cost[enddim];
  v = new cost[enddim];

  for (dim = startdim; dim <= enddim; dim++)
  {
    seedRandom(1000 * dim);
    /* in Visual C++ the first random numbers are not very random.
       call random couple of times before we really start. */
    //rrandom(); rrandom(); rrandom(); rrandom(); rrandom(); 
    /*for (i = 0; i < dim; i++)
      for (j = 0; j < dim; j++)
        assigncost[i][j] = (cost) 1; 
        //(rrandom() * (double) COSTRANGE);
    */
#if (PRINTCOST) 
    for (i = 0; i < dim; i++)
    {
      printf("\n");
      for (j = 0; j < dim; j++)
        printf("%4d ", assigncost[i][j]);
    }
#endif
    
    printf("\nstart\n");
    runtime = seconds();
    lapcost = lap(dim, assigncost, rowsol, colsol, u, v);
    runtime = seconds() - runtime;
    printf("\n\ndim  %4d \nlap cost %ld \nruntime %8.3f\n", dim, lapcost, runtime);
  
    checklap(dim, assigncost, rowsol, colsol, u, v);
  }

  delete[] assigncost;
  delete[] rowsol;
  delete[] colsol;
  delete[] u;
  delete[] v;

  //printf("\n\npress key\n");
  //char c;
  //scanf("%c", &c);
}

