#include "SpMatrix.h"
#include <limits> //for NAN type


//This entire file is new. --//ADAM

/*static bool
SpMatrix::CellCompare(incell first, incell second) {
  return (first.row_idx < second.row_idx) ||
    ((first.row_idx == second.row_idx) && (first.col_idx < second.col_idx));
}*/

void
SpMatrix::Resize(uint num_rows, uint num_edges) {
  row.clear();
  col.clear();
  cost.clear();
  row.resize(num_rows+1);
  col.resize(num_edges);
  cost.resize(num_edges);
  rowsize=num_rows;
}

double 
SpMatrix::GetCost(uint i, uint j) const {
  //cout << "GetCost called with indices " << i << "," << j << endl;
  //cout << "row.size() = " << row.size() << endl;
  if(i >= rowsize || j >= rowsize) {
    cerr << "SpMatrix::GetCost called with invalid indices" << endl;
    return std::numeric_limits<double>::quiet_NaN();
  }

  uint upper = row[i+1];
  uint lower = row[i];
  uint mid = (upper+lower)/2;
  while(upper > lower+1) {
    if(col[mid] > j) 
      upper = mid;
    else
      lower = mid;
    mid = (upper+lower)/2;
  }
  if(col[mid]==j) return cost[mid];
  else return std::numeric_limits<double>::quiet_NaN();
}

//ADAM:
//NOTE: This function is not used so far.
uint
SpMatrix::GetInd(uint i, uint j) const {
  uint upper = row[i+1];
  uint lower = row[i];
  uint mid = (upper+lower)/2;
  while(upper > lower+1) {
    if(col[mid] > j)
      upper = mid;
    else
      lower = mid;
    mid = (upper+lower)/2;
  }
  if(col[mid]==j) return mid;
  else return UINT_MAX;
}




