#include <stdio.h>
#include <iostream>
#include <string.h>
#include <math.h>
#include <vector>
//#include <omp.h>
#include <algorithm>
#include <fstream>
using namespace std;

typedef struct /* the graph data structure */
{
    long n; // numver of vertices in both sides
    long nrows; // number of vertices in the left side
    long m; // number of edges
    long* vtx_pointer; // an array of size n+1 storing the pointer in endV array
    long* endV; //an array of size m that stores the second vertex of an edge.
    double* weight; // not used in unweighted graph
} graph;


long* MS_BFS_Graft(graph* G, long* mateI);
long KarpSipserInitS(graph* G, long* unmatchedU,  long* mate);
void process_mtx_compressed(char *fname, graph* bGraph);
long* ms_BFS_Graft_main(char*);


