
#include "Assignment.h"
#include <limits> //for NAN type

void
Assignment::RandomGenerate(SpMatrix& spm, uint nrows, uint ncols, int Max, uint _seed){
 
  //accept new seed for random generator
  if(_seed != SEED)
    srand(_seed);
  else 
    srand(seed);
  
  //ADAM
  spm.row.clear();
  spm.col.clear();     
  spm.cost.clear();
  spm.row.resize(nrows+1);
  spm.col.resize(nrows*ncols);
  spm.cost.resize(nrows*ncols);
  spm.rowsize = nrows;
  for(uint i=0; i<nrows; i++) {
    spm.row[i]=ncols*i;
    for(uint j=0; j<ncols; j++) {
      spm.col[spm.row[i]+j]=j;
      int rdm=rand()%Max-1;
      if(rdm<0) rdm = 0;
      spm.cost[spm.row[i]+j] = rdm;
    }
  }
  spm.row[nrows]=nrows*ncols;

}

void
Assignment::RandomGenerate(uint nrows, uint ncols, int Max, uint _seed){

  RandomGenerate(spm, nrows, ncols, Max, _seed);
  //update data members
  row_size = nrows;
  col_size = ncols;
  
  //ADAM: number of edges
  edge_size = nrows*ncols;
}

bool
CellCompare(incell first, incell second) {
  return (first.row_idx < second.row_idx) ||
    ((first.row_idx == second.row_idx) && (first.col_idx < second.col_idx));
}

void
Assignment::ImportSparseMatrix(ifstream& input_file, char *fname, SpMatrix& spm){

  //for(int i=0; i<5; i++) {
  //  cout << init_assn[i] << endl;
  //}

  //
  //Read input from matrix market format.
  //
  string s;
  //long inp;

  //Ignore leading comments.
  while(input_file.peek()=='%') {
    getline(input_file,s);
    //cout << "Ignoring line: " << s << endl;
  }
  input_file>>row_size;
  input_file>>col_size;
  input_file>>edge_size;
  cout << row_size << " rows, " << col_size << " cols, " << edge_size << " edges." << endl;

  vector<incell> input_edges;
  input_edges.resize(edge_size);
  incell next_input_cell;
  for(uint i=0; i<edge_size; i++) {
    input_file>>next_input_cell.row_idx;
    input_file>>next_input_cell.col_idx;
    input_file>>next_input_cell.elt_wt;

    //Reindex from 0:
    next_input_cell.row_idx--;
    next_input_cell.col_idx--;

    //cout<< "Read line: " << next_input_cell.row_idx << " " << next_input_cell.col_idx << " " << next_input_cell.elt_wt << endl;
    input_edges[i]=next_input_cell;
  }
  std::sort(input_edges.begin(),input_edges.end(),CellCompare); 
  
  spm.row.resize(row_size+1);
  spm.col.resize(edge_size);
  spm.cost.resize(edge_size);
  spm.rowsize=row_size;

  spm.row[0]=0;
  spm.row[1]=0;
  uint current_row=0; 
  for(uint i=0; i<edge_size; i++) {
    spm.col[i]=input_edges[i].col_idx;
    spm.cost[i]=input_edges[i].elt_wt;
    while(current_row < input_edges[i].row_idx) {
      current_row++;
      spm.row[current_row+1] = spm.row[current_row];
    }
    spm.row[current_row+1]++;
    /*
    if(input_edges[i].row_idx==current_row) {
      spm.row[current_row+1]++;
    }
    else {
      current_row++;
      if(current_row<row_size)
        spm.row[current_row+1] = spm.row[current_row];
    }*/
  }

  /*for(uint i=0; i<=row_size;i++)
    cout << spm.row[i] << " ";
  cout << endl;
  for(uint i=0; i < edge_size; i++)
    cout << spm.col[i] << "\t\t";
  cout << endl;
  for(uint i=0; i < edge_size; i++) 
    cout << spm.cost[i] << "\t";
  cout << endl << endl;
  */

  long *init_assn = ms_BFS_Graft_main(fname);
  asgn_vec.resize(row_size);
  for(uint i=0; i< row_size; i++) {
    //Subtract row_size because ms_BFS_Graft adds it to every column id.
    asgn_vec[i]=init_assn[i]-row_size;
  }
  free(init_assn);

  //cout << "Proceeding!!" << endl << endl;
}

void
Assignment::ImportSparseMatrix(ifstream& input_file, char *fname){
  ImportSparseMatrix(input_file, fname, spm);
  //update class data members
  assert(spm.rowsize==spm.row.size()-1);
  row_size = spm.rowsize;
  col_size = row_size;
  edge_size= spm.cost.size();

}


void
Assignment::ImportMatrix(ifstream& input_file, SpMatrix& spm){
  cout << "Reading dense matrix from file." << endl; 
 
  string line;
  vector<double> numstream;

  uint num_rows = 0;
  uint num_cols = 0;

  if (input_file.is_open())
  {
    while (!input_file.eof() )
    {
      getline (input_file,line);

      uint local_num_cols=0;
      vector<double> local_numstream;
      string word;

      stringstream parse(line);
      while(parse >> word){
	//if comment line, ignore it
	if(word[0] == '#')
	  break;
	//numstream.push_back(atoi(word.c_str()));  //if not number, then convert to zero
	numstream.push_back(atof(word.c_str()));  //if not number, then convert to zero
	//local_numstream.push_back(atoi(word.c_str()));
	local_numstream.push_back(atof(word.c_str()));

	//matrix(num_rows, num_cols)= atoi(word.c_str()); 	
	local_num_cols++;
      } //end inner while

      //double check if the matrix format is correct or not
      if(num_cols && local_num_cols && num_cols!=local_num_cols){
	cerr<<endl<<"Please input a correct matrix format!"<<endl<<endl;
	exit(0);
      }
      //update column number if everything looks normal
      if(local_num_cols)
	num_cols = local_num_cols;
      //update row number if everything looks normal
      if(line.length()&&local_numstream.size())
        num_rows++;

    } //end out layer while

    input_file.close();

    assert(num_rows==num_cols);

    spm.row.resize(num_rows+1);
    spm.col.resize(num_rows*num_rows);
    spm.cost.resize(num_rows*num_rows);
    spm.rowsize=num_rows;
    //put elements into matrix
    //matrix.resize(num_rows, num_cols);
    //matrix.resize(num_rows);
    //for(uint i=0; i<num_rows; i++)
    //  matrix[i].resize(num_cols);

    vector<double>::iterator itr = numstream.begin();
    for(uint i=0; i<num_rows; i++) {
      spm.row[i]=i*num_cols;
      for(uint j=0; j<num_cols; j++) {
	spm.col[spm.row[i]+j]=j;
        spm.cost[spm.row[i]+j]=*itr++;
      }
    }
    spm.row[num_rows]=num_rows*num_cols;
  } //end outmost if
  else{ 
    cerr <<endl<<"Error: Unable to open file! Stopped."<<endl<<endl; 
    exit(0);
  }
  
}

void
Assignment::ImportMatrix(ifstream& input_file){

  ImportMatrix(input_file, spm);
  //update class data members
  assert(spm.rowsize==spm.row.size()-1);
  row_size = spm.rowsize;
  col_size = row_size;
  edge_size= spm.cost.size();
}


void
Assignment::ImportVec(ifstream& input_file, vector<uint>& vec){

  string line;

  if (input_file.is_open())
  {
    while (!input_file.eof() )
    {
      uint num_cols = 0;
      vec.clear();
      getline (input_file,line);
      string word;

      stringstream parse(line);
      while(parse >> word){
	//if comment line, ignore it
	if(word[0] == '#')
	  break;
	vec.push_back(atoi(word.c_str()));  //if not number, convert to zero
	num_cols++;
      } //end inner while

      if(num_cols > 0)
        break;

    } //end out layer while

    input_file.close();

  } //end outmost if
  else{ 
    cerr <<endl<<"Error: Unable to open file! Stopped."<<endl<<endl; 
    exit(0);
  }

}


void
Assignment::ImportVec(ifstream& input_file){

  ImportVec(input_file, asgn_vec);

}


//ADAM
void
Assignment::NegateMatrix(SpMatrix& sm) {
  for(uint i=0; i<sm.cost.size(); i++) {
    sm.cost[i]=-sm.cost[i];
  }
}

void
Assignment::NegateMatrix(void){

  NegateMatrix(spm);

}



void
Assignment::DisplayMatrix(SpMatrix& spm) const{

  if(spm.rowsize > DISPLAY_WIDTH*3){
    _cout("Matrix is big, not displaying."<<endl);
    return;
  }
  
  _cout(endl<<"The assignment problem (matrix) you queried is:"<<endl<<endl);
  assert(spm.rowsize == spm.row.size()-1);
  for(uint i=0; i<spm.rowsize; i++){
    for(uint j=0; j < spm.rowsize; j++)  
      _cout("  "<<spm.GetCost(i,j)<<"\t");
    _cout(endl);
  }
  _cout(endl);

  //cout << "ADAM end Assignment::DisplayMatrix\n";

}

//ADAM
/*double 
Assignment::GetSpCost(spmat& spm, uint i, uint j) {
  uint upper = spm.row[i+1];
  uint lower = spm.row[i];
  uint mid = (upper+lower)/2;
  while(upper > lower+1) {
    if(spm.col[mid] > j) 
      upper = mid;
    else
      lower = mid;
    mid = (upper+lower)/2;
  }
  if(spm.col[mid]==j) return spm.cost[mid];
  else return std::numeric_limits<double>::quiet_NaN();
}*/


