
///////////////////////////////////////////////////////////////////////////////
// File name: SpMatrix.h
// This file defines a class to represent sparse matrices. 
// Adam Sealfon, Jun 23, 2015
// Last modified: Jun 23, 2015
///////////////////////////////////////////////////////////////////////////////

#ifndef SP_MATRIX_H
#define SP_MATRIX_H

#include "define.h"
#include <stdint.h>
#include <climits>

using namespace std;


///////////////////////////////////////////////////////////////////////////////
//
// SpMatrix class: defined methods for obtaining an assignment matrix
//
///////////////////////////////////////////////////////////////////////////////

class SpMatrix{
public:
  SpMatrix(){}
  ~SpMatrix(){}
  
  //Sparse matix utilities:
  //bool CellCompare(incell, incell);


  void Resize(uint,uint);

  double GetCost(uint,uint) const;
  uint GetInd(uint,uint) const;

  vector<uint> row;
  vector<uint> col;
  vector<double> cost;

  uint rowsize;
  
};

#endif


